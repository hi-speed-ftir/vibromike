class Plotset:

    def __init__(self):
        self.source_plot = {}
        self.spectrum_plot = {}
        self.m2_func_plot = {}
        self.int_plot = {}
        self.ftir_plot = {}
        self.ndir_plot = {}
        self.ndir_int_plot = {}

        self.ndir_rounds = 0

        self.plots = {'Source': self.source_plot,
                      'Spectrum': self.spectrum_plot,
                      'M2': self.m2_func_plot,
                      'Int': self.int_plot,
                      'FTIR': self.ftir_plot,
                      'NDIR': self.ndir_plot}

    def set_plot(self, object, name): #object can be a Mike or a Spectrum
        if name == 'Source':
            self.source_plot = {'Difference': {'X': object.WN,
                                               'Y': 1 - (object.BB['BB_Irel'] - object.GC['GC_I']),
                                               'x_label': 'Frequency (cm-1)',
                                                'y_label': 'Relative intensity',
                                               'type': 'line',
                                               'label': 'Difference',
                                               'status': False},
                                'Black Body Source': {'X': object.WN,
                                                      'Y': object.BB['BB_Irel'],
                                                      'x_label': 'Frequency (cm-1)',
                                                      'y_label': 'Relative intensity',
                                                      'type': 'line',
                                                      'label': 'Black Body Source at {} K'.format(object.src_T),
                                                      'status': False},
                                'Absorption from Gas Cell': {'X': object.WN,
                                                             'Y': object.GC['GC_I'],
                                                             'x_label': 'Frequency (cm-1)',
                                                             'y_label': 'Differential spectrum',
                                                             'type': 'line',
                                                             'label': 'Absorption from Gas Cell',
                                                             'status': True}}
        elif name == 'Spectrum':
            self.spectrum_plot = {1: {'X': object.WN,
                                      'Y': object.spectrum_data['neg_spectrum'],
                                      'x_label': 'WN (cm-1)',
                                      'y_label': 'Absorbance',
                                      'type': 'line',
                                      'label': 'Absorbance spectrum',
                                      'status': True}}
        elif name == 'M2_func':
            self.m2_func_plot = {'Function': {'X': object.Time,
                                              'Y': object.M2_function['M2_Phi'],
                                              'x_label': 't (s)',
                                              'y_label': 'Amplitude (um)',
                                              'type': 'line',
                                              'label': 'Function',
                                              'status': True},
                                 'Detector Measurements': {'X': object.Time,
                                                           'Y': object.M2_function['M2_Phi'],
                                                           'x_label': 't (s)',
                                                           'y_label': 'Amplitude (um)',
                                                           'type': 'scatter',
                                                           'label': 'Detector Measurements',
                                                           'status': False}}
        elif name == 'Det':
            self.int_plot = {'At source': {'X': object.Time,
                                           'Y': object.Det_src['Sum'],
                                           'x_label': 't (s)',
                                           'y_label': 'Intensity at detector',
                                           'type': 'line',
                                           'label': 'Interferogram from source',
                                           'status': False},
                             'At Gas Cell': {'X': object.Time,
                                             'Y': object.Det_gc['Sum'],
                                             'x_label': 't (s)',
                                             'y_label': 'Intensity at detector',
                                             'type': 'line',
                                             'label': 'Interferogram from Gas cell',
                                             'status': False},
                             'Difference': {'X': object.Time,
                                             'Y': object.Det_diff['Diff'],
                                             'x_label': 't (s)',
                                             'y_label': 'Intensity at detector',
                                             'type': 'line',
                                             'label': 'Subtracted Interferogram',
                                             'status': True}}

        elif name == 'FTIR':
            self.ftir_plot = {'Src_ftir': {'X': object.FTIR_freqs_plot,
                                            'Y': object.ftir['Src_ftir'],
                                            'x_label': 'Frequency (cm-1)',
                                            'y_label': 'Absorbance',
                                            'type': 'line',
                                            'label': '',
                                            'status': False},
                              'GC_ftir': {'X': object.FTIR_freqs_plot,
                                           'Y': object.ftir['GC_ftir'],
                                           'x_label': 'Frequency (cm-1)',
                                           'y_label': 'Absorbance',
                                           'type': 'line',
                                           'label': '',
                                           'status': False},
                              'Log_ftir': {'X': object.FTIR_freqs_plot,
                                            'Y': object.ftir['Log_ftir'],
                                            'x_label': 'Frequency (cm-1)',
                                            'y_label': 'Absorbance',
                                            'type': 'line',
                                            'label': '',
                                            'status': False},
                              'Sub_ftir': {'X': object.FTIR_freqs_plot,
                                            'Y': object.ftir['Sub_ftir'],
                                            'x_label': 'Frequency (cm-1)',
                                            'y_label': 'Absorbance',
                                            'type': 'line',
                                            'label': '',
                                            'status': False},
                              'Sub_ftir_norm': {'X': object.FTIR_freqs_plot,
                                                 'Y': object.ftir['Sub_ftir_norm'],
                                                 'x_label': 'Frequency (cm-1)',
                                                 'y_label': 'Absorbance',
                                                 'type': 'line',
                                                 'label': '',
                                                 'status': True},
                              'Spectrum': {'X': object.WN,
                                           'Y': object.spectrum.spectrum_data['neg_spectrum'],
                                           'x_label': 'WN (cm-1)',
                                           'y_label': 'Absorbance',
                                           'type': 'line',
                                           'label': 'Absorbance spectrum',
                                           'status': True}}
        elif name == 'NDIR':
            self.ndir_rounds += 1
            self.ndir_plot[self.ndir_rounds] = {'Checked': True,
                                               'Sub_ndir': {'X': object.ndir['Freqs'],
                                                            'Y': object.ndir['Sub_ndir'],
                                                            'x_label': 'WN (cm-1)',
                                                            'y_label': 'Absorbance',
                                                            'type': 'line',
                                                            'label': 'Subtracted',
                                                            'status': True},
                                               'GC_ndir': {'X': object.ndir['Freqs'],
                                                          'Y': object.ndir['GC_ndir'],
                                                          'x_label': 'WN (cm-1)',
                                                          'y_label': 'Absorbance',
                                                          'type': 'line',
                                                          'label': 'NDIR at gas cell',
                                                          'status': False},
                                                'Src_ndir': {'X': object.ndir['Freqs'],
                                                            'Y': object.ndir['Src_ndir'],
                                                            'x_label': 'WN (cm-1)',
                                                            'y_label': 'Absorbance',
                                                            'type': 'line',
                                                            'label': 'At source',
                                                            'status': False}}
        elif name == 'NDIR_Int':
            self.ndir_int_plot[self.ndir_rounds] = {'Checked': True,
                                                    'NDIR_Sub_Int': {'X': object.Time,
                                                            'Y': object.NDIR_Int['Int_diff'],
                                                            'x_label': 't (s)',
                                                            'y_label': 'Intensity at detector',
                                                            'type': 'line',
                                                            'label': 'Subtracted',
                                                            'status': True}}

            for key in self.ndir_plot:
                self.ndir_plot.get(key)['Checked'] = False

            self.ndir_plot[list(self.ndir_plot.keys())[-1]]['Checked'] = True

