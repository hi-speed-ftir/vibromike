import pandas as pd
from PyQt5.QtWidgets import *
import sys
import model as m
import view as v
import plots as plts

App = QApplication(sys.argv)

class Controller:
    '''
    Main controller for the application
    Connects the Model to the View
    '''

    def __init__(self, window, parameters, molset, plotset, resultset):
        self.win = window
        self.parameters = parameters
        self.mols = molset
        self.resultset = resultset
        self.plotset = plotset

    def start_single(self):
        '''
        Starts a single simulation

        Creates a new Model object called Mike
        Updates the plotset
        Plots all the results
        Saves all the simulation results
        '''
        self.win.update_images()

        self.mike = m.Simulation(self.win, self.parameters, self.mols)
        self.mike.run()

        self.plotset.set_plot(self.mike, 'Source')
        self.win.plot('Source')

        self.plotset.set_plot(self.mike, 'M2_func')
        self.win.plot('M2_func')

        self.plotset.set_plot(self.mike, 'Det')
        self.win.plot('Det')

        self.plotset.set_plot(self.mike, 'FTIR')
        self.win.plot('FTIR')

        self.resultset.update_results(self.mols.current_mol, self.parameters, self.plotset, self.win.fct_slct.currentText())

        self.win.update_images()

    def open_graph(self, name, resultset):
        '''
        Opens a sub graph window for the designated plot when the user clicks an "open graph" button

        :param name: str
        :param resultset: resultset object
        :return: a Graph_window object
        '''
        if name == 'NDIR':
            self.graph_window = v.NDIR_graph_window(resultset)
        else:
            self.graph_window = v.Graph_window(name, resultset)

    def plot_m2_curve(self):
        '''
        Plots the vibration function using the input parameters without starting a new simualtion

        :return: a vibration function plot
        '''
        mike = m.Simulation(self.win, self.parameters, self.mols)
        mike.generate_M2_function()
        if self.win.fct_slct.currentText() == 'Square Wave':
            self.win.set_progress(100, "Square Wave Amplitude = {} µm".format(eval(self.parameters.m2_parameters_lines['Square Wave'].get('Amplitude').text())))

        self.plotset.set_plot(mike, 'M2_func')
        self.win.plot('M2_func')

    def plot_gas_spectrum(self, mol):
        '''
        Plots the theoretical spectrum of the selected molecule in the mol-slct dropdown

        :param mol: str
        :return: a theoretical spectrum plot
        '''
        spectrum = m.Spectrum(self.mols.molecules_data.get(mol), eval(self.parameters.simulation_parameters_lines.get('STD').text()))
        self.plotset.set_plot(spectrum, 'Spectrum')
        self.win.plot('Spectrum')

    def plot_ndir(self):
        '''
        Plots the NDIR spectrum

        :return: an NDIR spectrum in the spectrum graph
        '''
        self.mike.generate_ndir()
        self.plotset.set_plot(self.mike, 'NDIR')
        self.plotset.set_plot(self.mike, 'NDIR_Int')
        self.win.plot('NDIR')
        self.win.plot('NDIR_Int')
        self.resultset.update_ndir_plots(self.plotset)


if __name__ == '__main__':
    window = v.Window()
    sys.exit(App.exec())

