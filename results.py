class Resultset:

    def __init__(self):
        self.round = 0
        self.results = {}

    def update_results(self, molecule, parameters, plotset, fct):
        self.round += 1
        sim_parameters = {'Molecule': molecule,
                          'Concentration': parameters.test_parameters_lines.get('Concentration').text(),
                          'Cell length': parameters.test_parameters_lines.get('Cell length').text(),
                          'STD': parameters.simulation_parameters_lines.get('STD').text(),
                          'Detector Frequency': parameters.simulation_parameters_lines.get('Detector Frequency').text()}

        ndir_parameters = {'t min': parameters.ndir_parameters_lines.get('t min').text(),
                           't max': parameters.ndir_parameters_lines.get('t max').text()}

        function = {}
        if fct == 'Single Frequency':
            function = {'Function': 'SF',
                        'Amplitude': parameters.m2_parameters_lines['Single Frequency'].get('Amplitude').text(),
                        'Frequency': parameters.m2_parameters_lines['Single Frequency'].get('Frequency').text(),
                        "Secondary": '-'}

        elif fct == 'Amplitude Modulation':
            function = {'Function': 'AM',
                        'Amplitude': parameters.m2_parameters_lines['Amplitude Modulation'].get('Amplitude').text(),
                        'Main Frequency': parameters.m2_parameters_lines['Amplitude Modulation'].get('Main Frequency').text(),
                        'Carrier Frequency': parameters.m2_parameters_lines['Amplitude Modulation'].get('Carrier Frequency').text()}

        elif fct == 'Square Wave':
            function = {'Function': 'SW',
                        'Amplitude': parameters.m2_parameters_lines['Square Wave'].get('Amplitude').text(),
                        'Frequency': parameters.m2_parameters_lines['Square Wave'].get('Frequency').text(),
                        'k max': parameters.m2_parameters_lines['Square Wave'].get('k max').text()}

        plots = {'Source': plotset.source_plot,
                 'M2_func': plotset.m2_func_plot,
                 'Int': plotset.int_plot,
                 'FTIR': plotset.ftir_plot,
                 'NDIR': plotset.ndir_plot}

        self.results[self.round] = {'Checked': False,
                                    'Parameters': sim_parameters,
                                    'NDIR parameters': ndir_parameters,
                                    'Function': function,
                                    'Plots': plots}

        for key in self.results:
            self.results.get(key)['Checked'] = False
        self.results[list(self.results.keys())[-1]]['Checked'] = True

    def update_ndir_plots(self, plotset):
        for key in plotset.ndir_plot:
            plotset.ndir_plot.get(key)['Checked'] = False
        plotset.ndir_plot[list(plotset.ndir_plot.keys())[-1]]['Checked'] = True
        self.results[list(self.results.keys())[-1]]['Plots']['NDIR'] = plotset.ndir_plot

        for key in plotset.ndir_int_plot:
            plotset.ndir_int_plot.get(key)['Checked'] = False
        plotset.ndir_int_plot[list(plotset.ndir_int_plot.keys())[-1]]['Checked'] = True
        self.results[list(self.results.keys())[-1]]['Plots']['NDIR_Int'] = plotset.ndir_int_plot
