from PyQt5.QtWidgets import *
import PyQt5.QtWidgets
from PyQt5 import QtCore
from PyQt5.QtGui import QPixmap, QFont
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
import controller as c
from functools import partial
import molecules as mols
import parameters as params
import results as rslts
import plots as plts

class Window(QDialog):

    def __init__(self):
        super().__init__()

        ##Objects##
        self.parameters = params.Parameters()
        self.molset = mols.Molset()
        self.results = rslts.Resultset()
        self.plotset = plts.Plotset()
        self.controller = c.Controller(self, self.parameters, self.molset, self.plotset, self.results)

        ##Window Parameters##
        self.width = 1920
        self.height = 1080
        self.img_size = 200

        self.titles_Font = QFont('Helvetica', 16, QFont.Bold)
        self.sub_titles_Font = QFont('Helvetica', 10, QFont.Bold)

        # -Collections
        self.images = {}
        self.graph_windows = {}

        ##Simulation parameters##
        self.status = 'off'

        ##Initialize window##
        self.setWindowTitle("Vibro Mike")
        self.setGeometry(0, 0, self.width, self.height)
        self.make_window()
        self.showMaximized()
        self.show()

    def make_window(self):
        mainBox = QHBoxLayout()
        self.setLayout(mainBox)

        vbox_1 = QVBoxLayout()
        vbox_1.addWidget(self.test_parameters_box(), 33)
        vbox_1.addWidget(self.generate_img('source'), 33)
        vbox_1.addWidget(self.source_graph_box(), 33)

        vbox_2 = QVBoxLayout()
        vbox_2.addWidget(self.molecule_library_box(), 33)
        vbox_2.addWidget(self.generate_img('sample_cell'), 33)
        vbox_2.addWidget(self.int_graph_box(), 33)

        vbox_3 = QVBoxLayout()
        vbox_3.addWidget(self.m2_parameters_box(), 30)
        vbox_3.addWidget(self.generate_img('m2'), 10)
        vbox_3.addWidget(self.generate_img('sep'), 20)
        vbox_3.addWidget(self.generate_img('det'), 10)
        vbox_3.addWidget(self.ndir_parameters_box(), 30)

        vbox_4 = QVBoxLayout()
        vbox_4.addWidget(self.m2_graph_box(), 33)
        vbox_4.addWidget(self.generate_img('m1'), 33)
        vbox_4.addWidget(self.ftir_graph_box(), 33)

        mainBox.addLayout(vbox_1, 25)
        mainBox.addLayout(vbox_2, 25)
        mainBox.addLayout(vbox_3, 25)
        mainBox.addLayout(vbox_4, 25)

    def test_parameters_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Simulation Parameters')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        sim_label = QLabel('Environmental Conditions')
        sim_label.setFont(self.sub_titles_Font)
        vBox.addWidget(sim_label)
        for key in self.parameters.test_parameters:
            hBox = QHBoxLayout()
            label = QLabel()
            label.setText(key + ": ")
            self.line = QLineEdit(key)
            self.line.setText(str(self.parameters.test_parameters[key]))
            self.parameters.test_parameters_lines[key] = self.line
            hBox.addWidget(label)
            hBox.addWidget(self.line)
            vBox.addLayout(hBox)

        source_label = QLabel('Simulation Parameters')
        source_label.setFont(self.sub_titles_Font)
        vBox.addWidget(source_label)
        for key in self.parameters.simulation_parameters:
            hBox = QHBoxLayout()
            label = QLabel()
            label.setText(key + ": ")
            self.line = QLineEdit(key)
            self.line.setText(str(self.parameters.simulation_parameters[key]))
            self.parameters.simulation_parameters_lines[key] = self.line
            hBox.addWidget(label)
            hBox.addWidget(self.line)
            vBox.addLayout(hBox)

        self.start_btn = QPushButton('Simulation!')
        self.start_btn.clicked.connect(self.start_single)

        vBox.addWidget(self.start_btn)

        hbox = QHBoxLayout()
        log_lbl = QLabel('Log')
        log_lbl.setFont(self.sub_titles_Font)
        hbox.addWidget(log_lbl)
        self.progress = QProgressBar()
        hbox.addWidget(self.progress)

        vBox.addLayout(hbox)

        group.setLayout(vBox)

        return group

    def source_graph_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Source and Abs. Spectra')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)
        self.source_graph = MplWidget()
        self.graph_windows['Source'] = self.source_graph
        vBox.addWidget(self.source_graph)

        open_btn = QPushButton('Open Graph')
        part = partial(self.open_graph, 'Source')
        open_btn.clicked.connect(part)
        vBox.addWidget(open_btn)

        group.setLayout(vBox)

        return group

    def molecule_library_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Molecule Library')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        self.mol_slct = QComboBox()
        self.mol_slct.activated.connect(self.update_molecule)
        self.mol_slct.addItem('None')
        for key in self.molset.molecules_data:
            self.mol_slct.addItem(key)
        vBox.addWidget(self.mol_slct)

        self.mol_img_label = QLabel()
        self.mol_img_label.setAlignment(QtCore.Qt.AlignCenter)
        self.mol_img_label.setMaximumHeight(self.img_size)
        self.mol_img = QPixmap('images/None.png')
        self.mol_img = QPixmap('images/' + self.mol_slct.currentText() + '.png')
        self.scaled_mol_img = self.mol_img.scaledToHeight(self.mol_img_label.height())
        self.mol_img_label.setPixmap(self.scaled_mol_img)
        vBox.addWidget(self.mol_img_label)

        self.plt_spectrum_btn = QPushButton("Plot Spectrum")
        self.plt_spectrum_btn.clicked.connect(self.plot_spectrum)
        vBox.addWidget(self.plt_spectrum_btn)

        group.setLayout(vBox)

        return group

    def log_box(self):
        group = QGroupBox()
        self.vBox = QVBoxLayout()
        title = QLabel('Application log')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        self.log = QTextEdit()
        self.log.setReadOnly(True)
        self.log.setText("Hello, I'm µMike, a Michelson Interferometer Simulator \n")
        self.vBox.addWidget(title)
        self.vBox.addWidget(self.log)

        group.setLayout(self.vBox)

        return group

    def m2_parameters_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()
        title = QLabel('Vibrating Mirror Parameters')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        self.fct_slct = QComboBox()
        self.fct_slct.activated.connect(self.update_function)
        for key in self.parameters.m2_parameters:
            self.fct_slct.addItem(key)
            self.parameters.m2_parameters_lines[key] = {}
        vBox.addWidget(self.fct_slct)

        self.m2_fct_para_box = QVBoxLayout()

        for key in self.parameters.m2_parameters.get(self.fct_slct.currentText()):
            hBox = QHBoxLayout()
            label = QLabel()
            label.setText(key + ": ")
            self.line = QLineEdit(key)
            self.line.setText(str(self.parameters.m2_parameters[self.fct_slct.currentText()][key]))
            self.parameters.m2_parameters_lines[self.fct_slct.currentText()][key] = self.line
            hBox.addWidget(label)
            hBox.addWidget(self.line)
            self.m2_fct_para_box.addLayout(hBox)

        vBox.addLayout(self.m2_fct_para_box)

        plot_btn = QPushButton('Plot function')
        plot_btn.clicked.connect(self.plot_m2_function)

        vBox.addWidget(plot_btn)
        group.setLayout(vBox)

        return group

    def ndir_parameters_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()
        title = QLabel('NDIR Parameters')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        for key in self.parameters.ndir_parameters:
            hBox = QHBoxLayout()
            label = QLabel()
            label.setText(key + ": ")
            self.line = QLineEdit(key)
            self.line.setText(str(self.parameters.ndir_parameters[key]))
            self.parameters.ndir_parameters_lines[key] = self.line
            hBox.addWidget(label)
            hBox.addWidget(self.line)
            vBox.addLayout(hBox)

        plot_ndir_btn = QPushButton('Plot NDIR')
        plot_ndir_btn.clicked.connect(self.plot_ndir)

        vBox.addWidget(plot_ndir_btn)

        group.setLayout(vBox)

        return group

    def m2_graph_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Vibrating Mirror Control Function')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        self.m2_graph = MplWidget()
        self.graph_windows['M2_func'] = self.m2_graph
        vBox.addWidget(self.m2_graph)

        open_btn = QPushButton('Open Graph')
        part = partial(self.open_graph, 'M2_func')
        open_btn.clicked.connect(part)
        vBox.addWidget(open_btn)

        group.setLayout(vBox)

        return group

    def int_graph_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Interferograms')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        self.int_graph = MplWidget()
        self.graph_windows['Det'] = self.int_graph
        vBox.addWidget(self.int_graph)

        open_btn = QPushButton('Open Graph')
        part = partial(self.open_graph, 'Int')
        open_btn.clicked.connect(part)
        vBox.addWidget(open_btn)

        group.setLayout(vBox)

        return group

    def ftir_graph_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Infrared Spectrum')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        self.ftir_graph = MplWidget()
        self.graph_windows['FTIR'] = self.ftir_graph
        vBox.addWidget(self.ftir_graph)

        hbox = QHBoxLayout()
        ftir_btn = QPushButton('Open FTIR')
        ftir_part = partial(self.open_graph, 'FTIR')
        ftir_btn.clicked.connect(ftir_part)
        hbox.addWidget(ftir_btn)
        ndir_btn = QPushButton('Open NDIR')
        ndir_part = partial(self.open_graph, 'NDIR')
        ndir_btn.clicked.connect(ndir_part)
        hbox.addWidget(ndir_btn)

        vBox.addLayout(hbox)

        group.setLayout(vBox)

        return group

    def generate_img(self, name):
        self.img_label = QLabel()
        self.img_label.setAlignment(QtCore.Qt.AlignCenter)
        self.img_label.setMaximumHeight(self.img_size)
        filename = 'images/' + name + '_' + self.status + '.png'
        self.img = QPixmap(filename)
        self.scaled_img = self.img.scaledToHeight(self.img_label.height())
        self.img_label.setPixmap(self.scaled_img)
        self.images[name] = self.img_label

        return self.img_label

    def start_single(self):
        self.controller.start_single()

    def open_graph(self, name):
        self.controller.open_graph(name, self.results)

    def plot_m2_function(self):
        self.controller.plot_m2_curve()

    def update_images(self):
        if self.status == 'on':
            self.status = 'off'
        elif self.status == 'off':
            self.status = 'on'

        for key in self.images:
            filename = 'images/' + key + '_' + self.status + '.png'
            img = QPixmap(filename)
            scaled_img = img.scaledToHeight(self.images.get(key).height())
            self.images.get(key).setPixmap(scaled_img)

    def update_molecule(self):
        filename = 'images/' + self.mol_slct.currentText() + '.png'
        self.mol_img = QPixmap(filename)
        self.scaled_mol_img = self.mol_img.scaledToHeight((self.mol_img_label.height()))
        self.mol_img_label.setPixmap(self.scaled_mol_img)
        self.molset.current_mol = self.mol_slct.currentText()

    def update_function(self):
        for i in reversed(range(self.m2_fct_para_box.count())):
            for j in reversed(range(self.m2_fct_para_box.itemAt(i).count())):
                self.m2_fct_para_box.itemAt(i).itemAt(j).widget().setParent(None)

        for key in self.parameters.m2_parameters.get(self.fct_slct.currentText()):
            hBox = QHBoxLayout()
            label = QLabel()
            label.setText(key + ": ")
            self.line = QLineEdit(key)
            self.line.setText(str(self.parameters.m2_parameters[self.fct_slct.currentText()][key]))
            self.parameters.m2_parameters_lines[self.fct_slct.currentText()][key] = self.line
            hBox.addWidget(label)
            hBox.addWidget(self.line)
            self.m2_fct_para_box.addLayout(hBox)

    def plot_ndir(self):
        self.controller.plot_ndir()

    def set_progress(self, value, text):
        self.progress.setFormat(text)
        self.progress.setValue(value)

    def plot(self, type):
        for key in self.graph_windows:
            self.graph_windows.get(key).canvas.axes.clear()

        if type == 'Source':
            graph = self.source_graph
            data = self.plotset.source_plot
        elif type == 'Spectrum':
            graph = self.source_graph
            data = self.plotset.spectrum_plot
        elif type == 'M2_func':
            graph =self.m2_graph
            data = self.plotset.m2_func_plot
        elif type == 'Det':
            graph = self.int_graph
            data = self.plotset.int_plot
        elif type == 'FTIR':
            graph = self.ftir_graph
            graph.canvas.axes.set_xlim(400, 4000)
            data = self.plotset.ftir_plot
        elif type == 'NDIR':
            graph = self.ftir_graph
            data = self.plotset.ndir_plot[list(self.plotset.ndir_plot.keys())[-1]]
        elif type == 'NDIR_Int':
            graph = self.int_graph
            data = self.plotset.ndir_int_plot[list(self.plotset.ndir_int_plot.keys())[-1]]

        for key in data:
            if key == 'Checked':
                continue

            if data.get(key)['status'] == True:
                if data.get(key)['type'] == 'line':
                    graph.canvas.axes.plot(data.get(key)['X'],
                                           data.get(key)['Y'],
                                           label=data.get(key)['label'])
                elif data.get(key)['type'] == 'scatter':
                    graph.canvas.axes.scatter(data.get(key)['X'],
                                              data.get(key)['Y'],
                                              label=data.get(key)['label'],
                                              s=10,
                                              color='red')

            graph.canvas.axes.set_xlabel(data.get(key)['x_label'])
            graph.canvas.axes.set_ylabel(data.get(key)['y_label'])

        graph.canvas.axes.legend()
        graph.canvas.draw()

    def plot_spectrum(self):
        self.update_molecule()
        if self.molset.current_mol != 'None':
            self.set_progress(100, "Plotting " + str(self.molset.current_mol) + " spectrum")
            self.controller.plot_gas_spectrum(self.molset.current_mol)
        else:
            self.set_progress(0, "Choose molecules to plot!")

class MplWidget(PyQt5.QtWidgets.QWidget):

    def __init__(self, parent=None):
        PyQt5.QtWidgets.QWidget.__init__(self, parent)

        self.canvas = FigureCanvas(Figure())

        vertical_layout = PyQt5.QtWidgets.QVBoxLayout()
        vertical_layout.addWidget(self.canvas)

        self.canvas.axes = self.canvas.figure.add_subplot(111)
        self.setLayout(vertical_layout)

class Graph_window(QDialog):

    def __init__(self, name, resultset):
        super().__init__()

        self.setWindowTitle('Graph Window')
        self.setGeometry(0, 0, 800, 600)
        self.titles_Font = QFont('Helvetica', 16, QFont.Bold)
        self.sub_titles_Font = QFont('Helvetica', 10, QFont.Bold)

        self.name = name
        self.resultset = resultset
        self.sim_lines = {}
        self.data_lines = {}

        self.InitUI()
        # self.showMaximized()
        self.show()
        self.plot()

    def InitUI(self):
        mainVBox = QVBoxLayout()
        self.setLayout(mainVBox)

        graph_group = QGroupBox()
        top_hbox = QHBoxLayout()
        graph_vbox = QVBoxLayout()

        self.graph = MplWidget()
        self.graph_TB = NavigationToolbar(self.graph.canvas, self)
        graph_vbox.addWidget(self.graph)
        graph_vbox.addWidget(self.graph_TB)
        top_hbox.addLayout(graph_vbox, 80)

        graph_slc = self.graph_selecter()

        graph_group.setLayout(top_hbox)

        grid = self.data_grid()

        top_hbox.addWidget(graph_slc, 20)

        mainVBox.addWidget(graph_group, 90)
        mainVBox.addWidget(grid, 10)

    def data_grid(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Data available to plot')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        headers = self.headers()
        vBox.addLayout(headers)

        data_vbox = QVBoxLayout()

        for key in self.resultset.results:
            hbox = QHBoxLayout()

            check_box = QCheckBox()
            if self.resultset.results.get(key).get('Checked') == True:
                check_box.setChecked(True)
            check_box.toggled.connect(self.update_sim_checks)
            self.sim_lines[key] = check_box
            hbox.addWidget(check_box)

            round_label = QLabel(str(key))
            round_label.setAlignment(QtCore.Qt.AlignCenter)
            hbox.addWidget(round_label)

            for item in self.resultset.results.get(key).get('Parameters'):
                label = QLabel(str(self.resultset.results.get(key).get('Parameters').get(item)))
                label.setAlignment(QtCore.Qt.AlignCenter)
                hbox.addWidget(label)

            for item in self.resultset.results.get(key).get('Function'):
                label = QLabel(str(self.resultset.results.get(key).get('Function').get(item)))
                label.setAlignment(QtCore.Qt.AlignCenter)
                hbox.addWidget(label)

            data_vbox.addLayout(hbox)

        vBox.addLayout(data_vbox)
        group.setLayout(vBox)
        return group

    def headers(self):
        header_hbox = QHBoxLayout()

        columns = ['Sim. round', 'Molecule', 'Concentration', 'Cell length', 'STD', 'Detector Frequency', 'Function', 'Amplitude', 'Frequency', 'Carrier / k']

        checker = QLabel('Plot')
        checker.setFont(self.sub_titles_Font)
        header_hbox.addWidget(checker, 10)

        for item in columns:
            label = QLabel(item)
            label.setFont(self.sub_titles_Font)
            label.setAlignment(QtCore.Qt.AlignCenter)
            header_hbox.addWidget(label, 10)

        return header_hbox

    def graph_selecter(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel("Graphs")
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title, 5)

        vBox_2 = QVBoxLayout()

        data = self.resultset.results[list(self.resultset.results.keys())[-1]]['Plots'][self.name]

        for item in data:
            hbox = QHBoxLayout()
            check_box = QCheckBox()
            check_box.setChecked(data.get(item)['status'])
            check_box.toggled.connect(self.update_data_checks)
            self.data_lines[item] = check_box
            hbox.addWidget(check_box)

            label = QLabel(str(item))
            label.setFont(self.sub_titles_Font)
            hbox.addWidget(label)

            vBox_2.addLayout(hbox)

        vBox.addLayout(vBox_2, 95)

        group.setLayout(vBox)

        return group

    def update_sim_checks(self):
        for key in self.sim_lines:
            self.resultset.results.get(key)['Checked'] = self.sim_lines.get(key).isChecked()
        self.plot()

    def update_data_checks(self):
        for sim in self.sim_lines:
            for key in self.data_lines:
                self.resultset.results[sim]['Plots'][self.name][key]['status'] = self.data_lines.get(key).isChecked()
        self.plot()

    def plot(self):
        self.graph.canvas.axes.clear()

        for result in self.resultset.results:
            if self.resultset.results.get(result).get('Checked') == True:
                plot_data = self.resultset.results.get(result).get('Plots').get(self.name)

                for key in plot_data:
                    if plot_data.get(key)['status'] == True:
                        if plot_data.get(key)['type'] == 'line':
                            self.graph.canvas.axes.plot(plot_data.get(key)['X'],
                                                        plot_data.get(key)['Y'],
                                                        label='Simulation {} {}'.format(result, key))
                        elif plot_data.get(key)['type'] == 'scatter':
                            self.graph.canvas.axes.scatter(plot_data.get(key)['X'],
                                                        plot_data.get(key)['Y'],
                                                        label='Simulation {} {}'.format(result, key),
                                                        s=10,
                                                        color='red')

                        self.graph.canvas.axes.set_xlabel(plot_data.get(key)['x_label'])
                        self.graph.canvas.axes.set_ylabel(plot_data.get(key)['y_label'])

        if self.name == 'FTIR':
            self.graph.canvas.axes.set_xlim(400, 4000)

        self.graph.canvas.axes.legend()
        self.graph.canvas.draw()


class NDIR_graph_window(QDialog):

    def __init__(self, resultset):
        super().__init__()

        self.setWindowTitle('Graph Window')
        self.setGeometry(0, 0, 800, 600)
        self.titles_Font = QFont('Helvetica', 16, QFont.Bold)
        self.sub_titles_Font = QFont('Helvetica', 10, QFont.Bold)

        self.resultset = resultset
        self.sim_lines = {}
        self.data_lines = {}
        self.ndir_lines = {}

        self.InitUI()
        self.showMaximized()
        self.show()
        self.plot()

    def InitUI(self):
        mainVBox = QVBoxLayout()
        self.setLayout(mainVBox)

        graph_group = QGroupBox()
        top_hbox = QHBoxLayout()
        graph_vbox = QVBoxLayout()

        self.graph = MplWidget()
        self.graph_TB = NavigationToolbar(self.graph.canvas, self)
        graph_vbox.addWidget(self.graph)
        graph_vbox.addWidget(self.graph_TB)
        top_hbox.addLayout(graph_vbox, 80)

        graph_slc = self.graph_selecter()

        graph_group.setLayout(top_hbox)

        grid = self.ndir_grid()

        top_hbox.addWidget(graph_slc, 20)

        mainVBox.addWidget(graph_group, 90)
        mainVBox.addWidget(grid, 10)

    def graph_selecter(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel("Graphs")
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title, 5)

        vBox_2 = QVBoxLayout()

        data = self.resultset.results[list(self.resultset.results.keys())[-1]]['Plots']['NDIR'][1]

        for item in data:
            if item == 'Checked':
                continue

            hbox = QHBoxLayout()
            check_box = QCheckBox()
            check_box.setChecked(data.get(item)['status'])
            check_box.toggled.connect(self.update_data_checks)
            self.data_lines[item] = check_box
            hbox.addWidget(check_box)

            label = QLabel(str(item))
            label.setFont(self.sub_titles_Font)
            hbox.addWidget(label)

            vBox_2.addLayout(hbox)

        vBox.addLayout(vBox_2, 95)

        group.setLayout(vBox)

        return group

    def ndir_grid(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Data available to plot')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        headers = self.headers()
        vBox.addLayout(headers)

        data_vbox = QVBoxLayout()
        for key in self.resultset.results[list(self.resultset.results.keys())[-1]]['Plots']['NDIR']:
            hbox = QHBoxLayout()

            check_box = QCheckBox()
            if self.resultset.results[list(self.resultset.results.keys())[-1]]['Plots']['NDIR'].get(key)['Checked'] == True:
                check_box.setChecked(True)
            check_box.toggled.connect(self.update_ndir_checks)
            self.ndir_lines[key] = check_box
            hbox.addWidget(check_box)

            round_label = QLabel(str(key))
            round_label.setAlignment(QtCore.Qt.AlignCenter)
            hbox.addWidget(round_label)

            for item in self.resultset.results[list(self.resultset.results.keys())[-1]].get('Parameters'):
                label = QLabel(str(self.resultset.results[list(self.resultset.results.keys())[-1]].get('Parameters').get(item)))
                label.setAlignment(QtCore.Qt.AlignCenter)
                hbox.addWidget(label)

            for item in self.resultset.results[list(self.resultset.results.keys())[-1]].get('NDIR parameters'):
                label = QLabel(str(self.resultset.results[list(self.resultset.results.keys())[-1]].get('NDIR parameters').get(item)))
                label.setAlignment(QtCore.Qt.AlignCenter)
                hbox.addWidget(label)

            data_vbox.addLayout(hbox)

        vBox.addLayout(data_vbox)
        group.setLayout(vBox)
        return group

    def headers(self):
        header_hbox = QHBoxLayout()

        columns = ['NDIR round', 'Molecule', 'Concentration', 'Cell length', 'STD', 'Detector Frequency', 't min', 't max']

        checker = QLabel('Plot')
        checker.setFont(self.sub_titles_Font)
        header_hbox.addWidget(checker, 10)

        for item in columns:
            label = QLabel(item)
            label.setFont(self.sub_titles_Font)
            label.setAlignment(QtCore.Qt.AlignCenter)
            header_hbox.addWidget(label, 10)

        return header_hbox

    def plot(self):
        self.graph.canvas.axes.clear()

        data = self.resultset.results[list(self.resultset.results.keys())[-1]]['Plots']['NDIR']

        for item in data:
            if data.get(item)['Checked']:
                for key in data.get(item):
                    if key == 'Checked':
                        continue
                    else:
                        if data.get(item).get(key)['status']:
                            self.graph.canvas.axes.plot(data.get(item).get(key)['X'],
                                                        data.get(item).get(key)['Y'],
                                                        label='Simulation {} {}'.format(item, key))

        self.graph.canvas.axes.legend()
        self.graph.canvas.draw()

    def update_data_checks(self):
        pass

    def update_ndir_checks(self):
        pass
