import controller as c
import pandas as pd
import numpy as np
from numpy import trapz
import random

##Fundamentals##
#-Constants
h = 6.6260e-34 #Planck's Constant (J.s)
c = 2.299e8 #Speed of Light (m.s-1)
kB = 1.380e-23 #Boltzmann Constant (J.K-1)

#-Frequency range
nu_min = 400 #cm-1
nu_max = 4000 #cm-1

class Simulation:
    '''
    Simulation class

    Uses all the user inputs to run the different calculations and generates plot data
    '''

    def __init__(self, window, parameters, molset):

        ##Objects##
        self.molset = molset
        self.mol = molset.current_mol
        self.win = window
        self.parameters = parameters

        ##Parameters from user input##
        self.src_T = eval(self.parameters.simulation_parameters_lines.get('Source Temperature').text()) #Source temperature (K)
        self.Fs = eval(self.parameters.simulation_parameters_lines.get('Detector Frequency').text()) #Sampling rate of the detector (Hz)
        # self.n = eval(self.parameters.test_parameters_lines.get('Sample number (N)').text()) #Sample number factor
        self.n = 100
        self.gas_conc = eval(self.parameters.test_parameters_lines.get('Concentration').text())  # Concentration of the selected molecule in the Gas cell
        self.cell_length = eval(self.parameters.test_parameters_lines.get('Cell length').text())  # Optical path length (unit ?)
        self.gas_temp = eval(self.parameters.test_parameters_lines.get('Gas Temperature (K)').text())  # Gas temperature (K)
        self.std = eval(self.parameters.simulation_parameters_lines.get('STD').text())  # Spectrum STD as long as not correlated to source temp

        ##Derived parameters##
        #-Waves
        self.t_step = 1 / self.Fs  # Sample Time interval
        self.f0 = nu_min  # Lowest Freq
        self.N = int(self.n * self.Fs / self.f0)  # Number of Samples (related to the number of interferograms per time unit)
        self.f_step = self.Fs / self.N  # Freq interval

        #-Ranges
        self.WN = np.arange(nu_min, nu_max, 1)
        self.FCT = np.arange(1, 10, 2.5e-3)  # Factors of base frequency to build entire spectrum from 400 to 4000 cm-1
        self.Time = np.linspace(0, (self.N - 1) * self.t_step, self.N)  # Time domain
        self.Freqs = np.linspace(0, (self.N - 1) * self.f_step, self.N)  # Frequencies domain
        self.FTIR_freqs_plot = self.Freqs[0:int(self.N / 2 + 1)]

    def run(self):
        '''
        Runs through the different function in order to perform the calculations

        :return: a Model object
        '''
        self.generate_blackbody_spectrum()
        self.generate_gas_cell_spectrum()
        self.generate_waves()
        self.generate_M2_function()
        self.generate_det_data()
        self.generate_ftir()

    def generate_blackbody_spectrum(self):
        '''
        Generate Black Body spectrum BB_I = f(WN) at given T(K)

        :return: a DataFrame with the light intensity as a function of the wavenumber
        '''
        self.BB = pd.DataFrame()

        BB_I = []  # W.m-2.str-1.(cm-1)-1
        BB_Phis = []  # ?

        p = 0
        self.win.set_progress(p, 'Generating Source Spectrum...')
        for wn in self.WN:
            # i = (2 * h * c ** 2 * wn ** 3) / (np.exp((h * c * wn) / (kB * self.src_T)) - 1)
            i = (2e8 * h * c ** 2 * wn ** 3) / (np.exp((100 * h * c * wn) / (kB * self.src_T)) - 1) #Planck's Law with wavenumbers
            BB_I.append(i)
            BB_Phis.append(random.randint(-8, 8) / 16)

            p += 1
            self.win.set_progress((p * 100) / len(self.WN), 'Generating Source Spectrum...')

        self.BB['BB_I'] = BB_I
        self.BB['BB_Irel'] = BB_I / max(BB_I)
        self.BB['BB_Phi'] = BB_Phis

        self.win.set_progress(100, 'Source Spectrum Generated')

    def generate_gas_cell_spectrum(self): #from full calculated spectrum
        '''
        Uses the DFT output file to calculate the absorption lines of the molecule off of the Black Body spectrum

        :return: a DataFrame with the light intensity exiting the gas cell as a function of the wavenumber
        '''
        self.GC = pd.DataFrame()

        self.win.set_progress(100, 'Generating Gas Cell Spectrum...')

        if self.mol == 'None':
            self.GC['GC_I'] = self.BB['BB_Irel']
        else:
            self.spectrum = Spectrum(self.molset.molecules_data.get(self.mol), self.std)
            self.GC['GC_I'] = self.BB['BB_Irel'] * self.spectrum.spectrum_data['neg_norm_spectrum']

    def generate_waves(self):
        '''
        Generates 2*pi*w*t function for each wavelength of the BB source

        :return: a DataFrame
        '''
        self.Waves = pd.DataFrame()

        p = 0
        self.win.set_progress(p, 'Generating Wave data...')

        i = 0
        for fct in self.FCT:
            w = 2 * np.pi * fct * self.f0 * self.Time  # 2*pi*w*t
            self.Waves[self.WN[i]] = w
            i += 1

            p += 1
            self.win.set_progress((p * 100) / len(self.WN), 'Generating Wave data...')

        self.win.set_progress(100, 'Wave data generated.')

    def generate_M2_function(self):
        self.win.set_progress(0, 'Generating vibration function...')

        if self.win.fct_slct.currentText() == 'Single Frequency':
            self.sf_freq = eval(self.parameters.m2_parameters_lines['Single Frequency'].get('Frequency').text())
            self.sf_amp = eval(self.parameters.m2_parameters_lines['Single Frequency'].get('Amplitude').text())

            self.M2_function = pd.DataFrame()
            self.M2_function['M2_Phi'] = self.sf_amp * np.sin(2 * np.pi * self.sf_freq * self.Time)

        elif self.win.fct_slct.currentText() == 'Amplitude Modulation':
            self.am_main_freq = eval(self.parameters.m2_parameters_lines['Amplitude Modulation'].get('Main Frequency').text())  # Moving mirror main frequency of vibration (Hz)
            self.am_car_freq = eval(self.parameters.m2_parameters_lines['Amplitude Modulation'].get('Carrier Frequency').text())  # Moving mirror AM envelope frequency (Hz)
            self.am_amp = eval(self.parameters.m2_parameters_lines['Amplitude Modulation'].get('Amplitude').text())  # Amplitude of movement of moving mirror (um)

            self.M2_function = pd.DataFrame()
            main = np.cos(2 * np.pi * self.am_main_freq * self.Time)
            env = self.am_amp * np.sin(2 * np.pi * self.am_car_freq * self.Time)

            self.M2_function['M2_Phi'] = main * env

        elif self.win.fct_slct.currentText() == 'Square Wave':
            self.sw_amp = eval(self.parameters.m2_parameters_lines['Square Wave'].get('Amplitude').text())
            self.sw_freq = eval(self.parameters.m2_parameters_lines['Square Wave'].get('Frequency').text())
            self.sw_k = eval(self.parameters.m2_parameters_lines['Square Wave'].get('k max').text())

            self.M2_function = pd.DataFrame()

            for k in range(0, self.sw_k):
                self.M2_function[k] = self.sw_amp * np.sin((2 * k + 1) * 2 * np.pi * self.sw_freq * self.Time) / (2 * k + 1)

            self.M2_function['M2_Phi'] = 4/np.pi * self.M2_function.sum(axis=1)

        self.win.set_progress(100, 'Vibration function generated')

    def generate_det_data(self):
        # Generates the interferogramm in the form of A * (cos(wt) + cos(wt + phi)), where A is the light intensity at w wavelength
        self.Det_src = pd.DataFrame()
        self.Det_src['Time'] = self.Time
        self.Det_src['Freqs'] = self.Freqs
        self.Det_gc = pd.DataFrame()
        self.Det_gc['Time'] = self.Time
        self.Det_gc['Freqs'] = self.Freqs
        self.Det_diff = pd.DataFrame()
        self.Det_diff['Time'] = self.Time
        self.Det_diff['Freqs'] = self.Freqs

        ## 2pi wt: W_src/gc, Phi(t): M2_function

        p = 0
        self.win.set_progress(p, 'Generating Detector data...')

        i = 0
        for wn in self.WN:
            ##cos(wt + Phi) = cos(wt)cos(Phi) - sin(wt)sin(Phi)##

            y_src = self.BB['BB_Irel'][i] * (np.cos(self.Waves[wn]) +
                                             np.cos(self.Waves[wn] * np.cos(self.M2_function['M2_Phi'])) -
                                             np.sin(self.Waves[wn] * np.sin(self.M2_function['M2_Phi'])))
            y_gc = self.GC['GC_I'][i] * (np.cos(self.Waves[wn]) +
                                         np.cos(self.Waves[wn] * np.cos(self.M2_function['M2_Phi'])) -
                                         np.sin(self.Waves[wn] * np.sin(self.M2_function['M2_Phi'])))

            self.Det_src[self.WN[i]] = y_src
            self.Det_gc[self.WN[i]] = y_gc
            i += 1

            p += 1
            self.win.set_progress((p * 100) / len(self.Time), 'Generating Detector data...')

        self.Det_src['Sum'] = self.Det_src.drop(['Time', 'Freqs'], axis=1).sum(axis=1)
        self.Det_gc['Sum'] = self.Det_gc.drop(['Time', 'Freqs'], axis=1).sum(axis=1)

        self.Det_diff['Diff'] = self.Det_gc['Sum'] - self.Det_src['Sum']

        self.win.set_progress(100, 'Detector data generated.')

        area = trapz(self.Det_diff['Diff'], dx=10)
        print(area)

    def generate_ftir(self):
        self.win.set_progress(0, 'Fourier Transform...')
        self.ftir = pd.DataFrame()

        FFT_src = np.fft.fft(self.Det_src['Sum'])
        FFT_gc = np.fft.fft(self.Det_gc['Sum'])

        self.ftir['Src_ftir'] = np.real(FFT_src[0:int(self.N / 2 + 1)])
        self.ftir['GC_ftir'] = np.real(FFT_gc[0:int(self.N / 2 + 1)])

        self.ftir['Log_ftir'] = - abs(np.log10(self.ftir['GC_ftir'] / self.ftir['Src_ftir'])) # Absorbance law: -log(I/I0)
        self.ftir['Sub_ftir'] = self.ftir['GC_ftir'] - self.ftir['Src_ftir']
        self.ftir['Sub_ftir_norm'] = self.ftir['Sub_ftir'] / max(self.ftir['Sub_ftir'])


        self.win.set_progress(100, 'Simulation complete!')

    def generate_ndir(self):
        self.ndir = pd.DataFrame()

        self.NDIR_Int = pd.DataFrame()
        self.NDIR_Int['Time'] = self.Time

        t_min = eval(self.parameters.ndir_parameters_lines.get('t min').text())
        idx_min = np.abs(self.Time - t_min).argmin() #Finds the closest value in Time to the input t min
        t_max = eval(self.parameters.ndir_parameters_lines.get('t max').text())
        idx_max = np.abs(self.Time - t_max).argmin() #Finds the closest value in Time to the input t max

        fact = np.zeros(len(self.Time))

        for i in range(len(self.Time)):
            if i >= idx_min and i <= idx_max:
                fact[i] = 1

        self.NDIR_Int['factor'] = fact

        self.NDIR_Int['Int_src'] = self.Det_src['Sum'] * self.NDIR_Int['factor']
        self.NDIR_Int['Int_gc'] = self.Det_gc['Sum'] * self.NDIR_Int['factor']
        self.NDIR_Int['Int_diff'] = self.Det_diff['Diff'] * self.NDIR_Int['factor']

        # self.Int_src = self.Det_src[self.Time.tolist().index(self.Time[idx_min]):self.Time.tolist().index(self.Time[idx_max])] #Shrinks to interferogram to the selected time window
        # self.Int_gc = self.Det_gc[self.Time.tolist().index(self.Time[idx_min]):self.Time.tolist().index(self.Time[idx_max])] #Shrinks to interferogram to the selected time window

        # FFT_src = np.fft.fft(self.Int_src['Sum'])
        # FFT_gc = np.fft.fft(self.Int_gc['Sum'])

        # self.ndir['Freqs'] = self.Int_src['Freqs'][0:int(self.N / 2 + 1)]
        self.ndir['Freqs'] = self.Freqs[0:int(self.N / 2 + 1)]

        FFT_src = np.fft.fft(self.NDIR_Int['Int_src'])
        FFT_gc = np.fft.fft(self.NDIR_Int['Int_gc'])

        self.ndir['Src_ndir'] = np.real(FFT_src[0:int(self.N / 2 + 1)])
        self.ndir['GC_ndir'] = np.real(FFT_gc[0:int(self.N / 2 + 1)])
        self.ndir['Sub_ndir'] = self.ndir['GC_ndir'] - self.ndir['Src_ndir']


class Spectrum:

    def __init__(self, mol_data, std):
        self.modes_data = mol_data
        self.std = std

        self.WN = np.arange(nu_min, nu_max, 1)

        ##Calculate spectrum graph##
        # self.drifted_modes = self.apply_drift()
        self.generate_spectrum_line()

    # def apply_drift(self):
    #     data = self.modes_data
    #     for key in data:
    #         data[key][0] = data[key][0] + self.spectrum_parameters.get('Drift')
    #
    #     return data

    def generate_spectrum_line(self):
        self.spectrum_data = pd.DataFrame()

        def gauss(list, std):
            V = []

            for wn in self.WN:
                v = list[1] / (std * np.sqrt(2 * np.pi)) * np.exp(-(wn - list[0]) ** 2 / (2 * std ** 2))
                V.append(round(v, 6))

            return V

        for key in self.modes_data:
            self.spectrum_data[key] = gauss(self.modes_data.get(key), self.std)

        self.spectrum_data['spectrum'] = self.spectrum_data.sum(axis=1)
        self.spectrum_data['norm_spectrum'] = self.spectrum_data['spectrum'] / max(self.spectrum_data['spectrum'])
        self.spectrum_data['neg_norm_spectrum'] = - self.spectrum_data['norm_spectrum'] + 1
        self.spectrum_data['neg_spectrum'] = - self.spectrum_data['spectrum']

    # def add_noise(self):
    #     noisy_spectrum = []
    #     max_noise = self.spectrum_parameters.get('Noise') * max(self.spectrum_data['spectrum'])
    #     for i in range(len(self.spectrum_data)):
    #         # data['noisy spectrum'][i] = data['humid spectrum'][i] + random.uniform(0.01, max_noise)
    #         noisy_spectrum.append(self.spectrum_data.get('spectrum')[i] + random.uniform(0.01, max_noise))
    #     self.spectrum_data['noisy spectrum'] = noisy_spectrum
    #     self.spectrum_data['neg noisy spectrum'] = -self.spectrum_data['noisy spectrum']
    #
    # def add_water(self): #TODO Implement adding humidity interference in the spectrum
    #     pass
