class Parameters:

    def __init__(self):
        self.test_parameters = {'Pressure': 1,
                                'Gas Temperature (K)': 300,
                                'Concentration': 1,
                                'Cell length': 1}
        self.test_parameters_lines = {}

        self.simulation_parameters = {'Source Temperature': 500,
                                      'STD': 25,
                                      'Detector Frequency': 50000}
        self.simulation_parameters_lines = {}

        self.ndir_parameters = {'t min': 0.137,
                                't max': 0.147}
        self.ndir_parameters_lines = {}

        self.m2_parameters = {'Single Frequency': {'Frequency': 20,
                                                   'Amplitude': 100},
                              'Amplitude Modulation': {'Main Frequency': 20,
                                                       'Amplitude': 100,
                                                       'Carrier Frequency': 2},
                              'Square Wave': {'Frequency': 2,
                                              'Amplitude': 3,
                                              'k max': 15}}
        self.m2_parameters_lines = {}

        self.live_status = False

